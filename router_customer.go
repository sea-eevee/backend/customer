package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/customer/api_customer"
	"gitlab.com/sea-eevee/backend/customer/uc_customer"
	"net/http"
)

func NewRouterCustomer(ucCustomer uc_customer.Contract) *mux.Router {

	r := mux.NewRouter()

	r.Methods(http.MethodGet).Path("/ping").HandlerFunc(ping)
	profile := r.PathPrefix("/profile").Subrouter()
	{
		profile.Methods(http.MethodGet).
			Handler(api_customer.ProfileSee(ucCustomer.ProfileSee))

		profile.Methods(http.MethodPut).
			Handler(api_customer.ProfileUpdate(ucCustomer.ProfileUpdate))
	}

	merchant := r.PathPrefix("/merchant").Subrouter()
	{
		merchant.Methods(http.MethodGet).Path(fmt.Sprintf("/{%s:[0-9]+}", api_customer.URLKeyMerchantID)).
			Handler(api_customer.MerchantReadOne(ucCustomer.MerchantReadOne))

		merchant.Methods(http.MethodGet).
			Handler(api_customer.MerchantReadMany(ucCustomer.MerchantReadMany))

	}

	item := r.PathPrefix("/item").Subrouter()
	{
		item.Methods(http.MethodGet).Path(fmt.Sprintf("/{%s:[0-9]+}", api_customer.URLKeyItemID)).
			Handler(api_customer.ItemReadOne(ucCustomer.MerchantItemReadOne))
	}

	cart := r.PathPrefix("/cart").Subrouter()
	{
		cart.
			Methods(http.MethodPost).
			Handler(api_customer.MyCartItemAdd(ucCustomer.MyCartItemAdd))

		cart.
			Methods(http.MethodGet).
			Handler(api_customer.MyCartRead(ucCustomer.MyCartRead))

		cart.Methods(http.MethodPut).
			Handler(api_customer.MyCartItemItemUpdateQuantity(ucCustomer.MyCartItemUpdateQuantityOne))
	}
	order := r.PathPrefix("/order").Subrouter()
	{
		order.
			Methods(http.MethodGet).
			Handler(api_customer.MyOrderReadMany(ucCustomer.MyOrderReadMany))
		order.
			Methods(http.MethodPost).
			Handler(api_customer.MyOrderCreate(ucCustomer.MyOrderCreate))
	}

	transaction := r.PathPrefix("/transaction").Subrouter()
	{
		transaction.
			Methods(http.MethodGet).
			Handler(api_customer.MyTransactionReadMany(ucCustomer.MyTransactionReadMany))
		transaction.
			Methods(http.MethodPost).
			Handler(api_customer.MyTransactionCreate(ucCustomer.MyTransactionCreate))
	}

	return r
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong from customer servcie")
}
