package api_customer

import (
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/customer/pkg/responder"
	"gitlab.com/sea-eevee/backend/customer/uc_customer"
	"net/http"
	"strconv"
)

const URLKeyItemID = "item_id"

func ItemReadOne(ucFunc uc_customer.MerchantItemReadOneUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)[URLKeyItemID]
		merchantID, err := strconv.ParseUint(vars, 10, 64)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		ts, err := ucFunc(&uc_customer.MerchantItemReadOneParam{ItemID: merchantID})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
