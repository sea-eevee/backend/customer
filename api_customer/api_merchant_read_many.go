package api_customer

import (
	"gitlab.com/sea-eevee/backend/customer/pkg/responder"
	"gitlab.com/sea-eevee/backend/customer/uc_customer"
	"net/http"
)

func MerchantReadMany(ucFunc uc_customer.MerchantReadManyUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		params := new(uc_customer.MerchantReadManyParam)
		ts, err := ucFunc(params)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
