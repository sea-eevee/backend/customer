package api_customer

import (
	"gitlab.com/sea-eevee/backend/common/request_getter"
	"gitlab.com/sea-eevee/backend/customer/pkg/responder"
	"gitlab.com/sea-eevee/backend/customer/uc_customer"
	"net/http"
)

func MyOrderReadMany(ucFunc uc_customer.MyOrderReadManyUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		uid, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		ts, err := ucFunc(&uc_customer.MyOrderReadManyParam{CustomerID: uid})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
