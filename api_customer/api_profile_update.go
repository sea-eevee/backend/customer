package api_customer

import (
	"encoding/json"
	"gitlab.com/sea-eevee/backend/common/request_getter"
	"gitlab.com/sea-eevee/backend/customer/pkg/errs"
	"gitlab.com/sea-eevee/backend/customer/pkg/responder"
	"gitlab.com/sea-eevee/backend/customer/uc_customer"
	"net/http"
)

func ProfileUpdate(ucFunc uc_customer.ProfileUpdateUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		uid, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		params := new(uc_customer.ProfileUpdateParam)
		err = json.NewDecoder(r.Body).Decode(params)
		if err != nil {
			responder.ResponseError(w, errs.ErrBadRequest)
			return
		}

		ts, err := ucFunc(&uc_customer.ProfileUpdateParam{
			CustomerID:  uid,
			DisplayName: params.DisplayName,
			Address:     params.Address,
		})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
