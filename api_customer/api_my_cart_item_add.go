package api_customer

import (
	"encoding/json"
	"gitlab.com/sea-eevee/backend/common/request_getter"
	"gitlab.com/sea-eevee/backend/customer/pkg/errs"
	"gitlab.com/sea-eevee/backend/customer/pkg/responder"
	"gitlab.com/sea-eevee/backend/customer/uc_customer"
	"net/http"
)

func MyCartItemAdd(ucFunc uc_customer.MyCartItemAddUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		params := new(uc_customer.MyCartItemAddParam)
		err := json.NewDecoder(r.Body).Decode(params)
		if err != nil {
			responder.ResponseError(w, errs.ErrBadRequest)
			return
		}

		uid, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		params.CustomerID = uid

		ts, err := ucFunc(params)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
