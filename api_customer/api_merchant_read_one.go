package api_customer

import (
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/customer/pkg/responder"
	"gitlab.com/sea-eevee/backend/customer/uc_customer"
	"net/http"
	"strconv"
)

const URLKeyMerchantID = "merchant_id"

func MerchantReadOne(ucFunc uc_customer.MerchantReadOneUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)[URLKeyMerchantID]
		merchantID, err := strconv.ParseUint(vars, 10, 64)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		ts, err := ucFunc(&uc_customer.MerchantReadOneParam{MerchantID: merchantID})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
