.PHONY : network-create db-reload app-build app-run app-stop app-clean

network:
	docker network inspect compfest-marketplace >/dev/null 2>&1 || \
        docker network create --driver bridge compfest-marketplace

db-reload:
	docker container stop compfest-marketplace-service-customer-db || true
	docker build -t compfest-marketplace-service-customer-db-img -f Dockerfile.postgres .
	docker run -d --rm --name compfest-marketplace-service-customer-db \
        --network=compfest-marketplace \
        -p 13300:5432 \
        -v compfest-marketplace-customer-vol:/var/lib/postgresql/data \
        -e POSTGRES_USER=pgku \
        -e POSTGRES_PASSWORD=pgku \
        -e POSTGRES_DB=customer \
        compfest-marketplace-service-customer-db-img

app-build:
	docker build -t compfest-marketplace-service-customer -f Dockerfile.app .

app-run:
	docker run -d --rm --name compfest-marketplace-service-customer \
	--network=compfest-marketplace \
	-p 8300:8300 \
	compfest-marketplace-service-customer

app-stop:
	docker stop compfest-marketplace-service-customer || true

app-reload:
	make app-stop
	make app-build
	make app-run

app-clean:
	docker rmi -f compfest-marketplace-service-customer

full-reload:
	make db-reload
	make app-reload