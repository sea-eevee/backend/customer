package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/backend/common/config"
	"gitlab.com/sea-eevee/backend/common/store"
	"gitlab.com/sea-eevee/backend/customer/repo/repo_customer"
	"gitlab.com/sea-eevee/backend/customer/repo/repo_merchant"
	"gitlab.com/sea-eevee/backend/customer/repo/repo_oms"
	"gitlab.com/sea-eevee/backend/customer/uc_customer"
	"log"
	"net/http"
	"os"
)

func main() {
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	cfg := config.InitAppConfig(path)
	repoCustomer, err := repo_customer.NewRepoCustomer(store.DBConn{
		DBHost: "compfest-marketplace-service-customer-db",
		DBPort: 5432,
		//DBHost: "localhost",
		//DBPort: 13300,
		DBUser: cfg.DBUser,
		DBPass: cfg.DBPass,
		DBName: "customer",
	})
	if err != nil {
		log.Fatal(err)
	}
	repoMerchant, err := repo_merchant.NewRepoMerchant(store.DBConn{
		DBHost: "compfest-marketplace-service-merchant-db",
		DBPort: 5432,
		//DBHost: "localhost",
		//DBPort: 13200,
		DBUser: cfg.DBUser,
		DBPass: cfg.DBPass,
		DBName: "merchant",
	})
	if err != nil {
		log.Println("ccc")
		log.Fatal(err)
	}
	repoOMS, err := repo_oms.NewRepoOMS(store.DBConn{
		DBHost: "compfest-marketplace-service-oms-db",
		DBPort: 5432,
		//DBHost: "localhost",
		//DBPort: 13400,
		DBUser: cfg.DBUser,
		DBPass: cfg.DBPass,
		DBName: "oms",
	})
	if err != nil {
		log.Fatal(err)
	}

	ucCustomer := uc_customer.NewUCCustomer(uc_customer.UCCustomerParam{
		RepoCustomer: repoCustomer,
		RepoMerchant: repoMerchant,
		RepoOMS:      repoOMS,
	})
	r := NewRouterCustomer(ucCustomer)
	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		tpl, err1 := route.GetPathTemplate()
		met, err2 := route.GetMethods()
		fmt.Println(tpl, err1, met, err2)
		return nil
	})

	http.ListenAndServe(":8300", r)
}
