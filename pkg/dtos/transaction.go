package dtos

import "time"

type Transaction struct {
	ID         uint64     `json:"id"`
	Customer   *Customer  `json:"customer"`
	Item       *Item      `json:"item"`
	Merchant   *Merchant  `json:"merchant"`
	IsAccepted *bool      `json:"is_accepted"`
	CreatedAt  *time.Time `json:"created_at"`
	IsPaid     *bool      `json:"is_paid"`
}
