package dtos

type Merchant struct {
	MerchantID  uint64   `json:"merchant_id"`
	DisplayName string   `json:"display_name"`
	Location    Location `json:"location"`
	Description string   `json:"description"`
	PhotoURL    string   `json:"photo_url"`
}

type Location struct {
	LocationID uint64 `json:"location_id"`
	Province   string `json:"province"`
	City       string `json:"city"`
}
