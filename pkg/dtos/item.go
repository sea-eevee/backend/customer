package dtos

type Item struct {
	ID          uint64 `json:"id"`
	MerchantID  uint64 `json:"merchant_id"`
	DisplayName string `json:"display_name"`
	Description string `json:"description"`
	Price       uint   `json:"price"`
	Quantity    uint   `json:"quantity"`
	Image       string `json:"image"`
}
