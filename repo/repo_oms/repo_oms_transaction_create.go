package repo_oms

func (r repoOMS) TransactionCreate(orderID uint64, bankName, bankAccountNumber string) error {
	query := `INSERT INTO transaction(order_id, bank_name, bank_account_number) VALUES ($1, $2, $3)`

	_, err := r.db.Exec(query, orderID, bankName, bankAccountNumber)
	if err != nil {
		return err
	}
	return nil
}
