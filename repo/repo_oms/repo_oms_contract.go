package repo_oms

import (
	"database/sql"
	"gitlab.com/sea-eevee/backend/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_oms . Contract

type Contract interface {
	MerchantItemStockReadOne(merchantID uint64) (*MerchantItemStock, error)

	OrderCreate(customerID, merchantID uint64, orderDetail []*OrderDetail) error
	OrderReadMany(customerID uint64) ([]*CustomerOrder, error)

	TransactionCreate(orderID uint64, bankName, bankAccountNumber string) error
	TransactionReadMany(customerID uint64) ([]*CustomerTransaction, error)
	OrderUpdatePay(orderID uint64) error
}

type repoOMS struct {
	db *sql.DB
}

func NewRepoOMS(param store.DBConn) (Contract, error) {
	db, err := store.NewPostgres(param)
	if err != nil {
		return nil, err
	}
	return &repoOMS{
		db: db,
	}, nil
}
