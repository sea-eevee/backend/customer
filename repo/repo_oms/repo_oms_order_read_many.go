package repo_oms

import "database/sql"

type CustomerOrder struct {
	OrderID      uint64         `json:"order_id"`
	CustomerID   uint64         `json:"customer_id"`
	OrderDetails []*OrderDetail `json:"order_details"`
	CreatedAt    uint64         `json:"created_at"`
	IsPaid       bool           `json:"is_paid"`
}

func (r repoOMS) OrderReadMany(customerID uint64) ([]*CustomerOrder, error) {
	var mw []*CustomerOrder

	const queryOrder = `SELECT DISTINCT o.id, o.customer_id, o.is_paid, order_date
					FROM orders o JOIN order_detail od on o.id = od.order_id 
					WHERE o.customer_id = $1`

	const queryOrderDetail = `SELECT order_id, item_id, quantity
								FROM order_detail
								WHERE order_id = $1`

	rows, err := r.db.Query(queryOrder, customerID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(CustomerOrder)
		err := rows.Scan(&m.OrderID, &m.CustomerID, &m.IsPaid, &m.CreatedAt)
		if err != nil {
			return nil, err
		}

		rowsDetail, err := r.db.Query(queryOrderDetail, m.OrderID)
		if err != nil {
			return nil, err
		}
		var dtList []*OrderDetail
		for rowsDetail.Next() {
			dt := new(OrderDetail)
			err := rowsDetail.Scan(&dt.OrderID, &dt.ItemID, &dt.Quantity)
			if err != nil {
				return nil, err
			}
			dtList = append(dtList, dt)
		}

		if err != nil {
			return nil, err
		}

		m.OrderDetails = dtList
		mw = append(mw, m)
	}

	return mw, nil
}
