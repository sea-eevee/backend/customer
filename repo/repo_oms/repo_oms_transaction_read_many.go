package repo_oms

import "database/sql"

type CustomerTransaction struct {
	TransactionID     uint64 `json:"transaction_id"`
	CustomerID        uint64 `json:"customer_id"`
	BankName          string `json:"bank_name"`
	BankAccountNumber string `json:"bank_account_number"`
	AdminApproval     *bool  `json:"admin_approval"`
	TransactionDate   uint64 `json:"transaction_date"`
}

func (r repoOMS) TransactionReadMany(customerID uint64) ([]*CustomerTransaction, error) {
	var mw []*CustomerTransaction

	const query = `SELECT 
       					o.id, o.customer_id, 
       					t.bank_name, t.bank_account_number, t.transaction_date, t.admin_approval
					FROM transaction t JOIN orders o on o.id = t.order_id
					WHERE  o.customer_id = $1 AND o.is_paid = TRUE`

	rows, err := r.db.Query(query, customerID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(CustomerTransaction)
		err := rows.Scan(&m.TransactionID, &m.CustomerID,
			&m.BankName, &m.BankAccountNumber, &m.TransactionDate, &m.AdminApproval)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
