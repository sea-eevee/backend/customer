package repo_oms

type OrderDetail struct {
	OrderID  uint64 `json:"order_id"`
	ItemID   uint64 `json:"item_id"`
	Quantity uint   `json:"quantity"`
}

func (r repoOMS) OrderCreate(customerID, merchantID uint64, orderDetail []*OrderDetail) error {

	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	var insertedID int
	queryOrderCreate := `INSERT INTO orders(customer_id, merchant_id) VALUES ($1, $2) RETURNING id`
	row := tx.QueryRow(queryOrderCreate, customerID, merchantID)
	err = row.Scan(&insertedID)
	if err != nil {
		tx.Rollback()
		return err
	}

	queryOrderDetail := `INSERT INTO order_detail(order_id, item_id, quantity) VALUES ($1, $2, $3)`
	for _, od := range orderDetail {
		_, err = tx.Exec(queryOrderDetail, insertedID, od.ItemID, od.Quantity)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}

	return nil
}
