package repo_oms

func (r repoOMS) OrderUpdatePay(orderID uint64) error {
	query := `UPDATE orders SET is_paid = true WHERE id = $1`
	_, err := r.db.Exec(query, orderID)
	return err
}
