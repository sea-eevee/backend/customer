package repo_customer

func (r repoCustomer) MyCartItemAdd(userID, itemID uint64, quantity uint) error {
	query := "INSERT INTO customer_shopping_cart(customer_id, item_id, quantity) VALUES($1,$2, $3)"
	_, err := r.db.Exec(query, userID, itemID, quantity)
	if err != nil {
		return err
	}
	return nil
}

func (r repoCustomer) MyCartRemove(customerID uint64) error {
	query := "DELETE FROM customer_shopping_cart WHERE customer_id = $1"
	_, err := r.db.Exec(query, customerID)
	if err != nil {
		return err
	}
	return nil
}

func (r repoCustomer) MyCartItemUpdateOne(customerID uint64, itemID uint64, diff int) error {
	query := `UPDATE customer_shopping_cart 
				SET quantity = quantity + $3
				WHERE customer_id = $1 AND item_id = $2`
	_, err := r.db.Exec(query, customerID, itemID, diff)
	if err != nil {
		return err
	}

	queryDel := `DELETE FROM customer_shopping_cart WHERE quantity = 0;`
	_, err = r.db.Exec(queryDel)
	if err != nil {
		return err
	}
	return nil
}
