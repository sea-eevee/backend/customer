package repo_customer

func (r repoCustomer) ProfileUpdate(customerID uint64, displayName, address string) error {
	query := `UPDATE customer_profile
				SET display_name = $2, address = $3
				WHERE customer_id = $1`

	_, err := r.db.Exec(query, customerID, displayName, address)
	if err != nil {
		return err
	}
	return nil
}
