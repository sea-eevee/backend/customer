package repo_customer

import (
	"database/sql"
)

type CustomerCart struct {
	ID         uint64
	CustomerID uint64
	ItemID     uint64
	Quantity   uint
}

func (r repoCustomer) MyCartRead(customerID uint64) ([]*CustomerCart, error) {
	var ts []*CustomerCart

	const query = `
		SELECT id, customer_id, item_id, quantity
		FROM customer_shopping_cart
		WHERE customer_id = $1`

	rows, err := r.db.Query(query, customerID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		t := new(CustomerCart)
		err := rows.Scan(&t.ID, &t.CustomerID, &t.ItemID, &t.Quantity)
		if err != nil {
			return nil, err
		}
		ts = append(ts, t)
	}

	return ts, nil
}
