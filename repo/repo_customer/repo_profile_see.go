package repo_customer

import "database/sql"

type CustomerProfile struct {
	CustomerID  uint64 `json:"customer_id"`
	DisplayName string `json:"display_name"`
	Address     string `json:"address"`
}

func (r repoCustomer) ProfileSee(customerID uint64) (*CustomerProfile, error) {
	const query = `SELECT customer_id, display_name, address
					FROM customer_profile 
					WHERE customer_id = $1`

	row := r.db.QueryRow(query, customerID)

	m := new(CustomerProfile)
	err := row.Scan(&m.CustomerID, &m.DisplayName, &m.Address)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return m, nil
}
