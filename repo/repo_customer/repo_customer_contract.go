package repo_customer

import (
	"database/sql"
	_ "github.com/lib/pq"
	"gitlab.com/sea-eevee/backend/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_customer . Contract

type Contract interface {
	ProfileUpdate(customerID uint64, displayName, address string) error
	ProfileSee(customerID uint64) (*CustomerProfile, error)

	MyCartItemAdd(customerID, itemID uint64, quantity uint) error
	MyCartItemUpdateOne(customerID uint64, itemID uint64, diff int) error
	MyCartRemove(customerID uint64) error
	MyCartRead(customerID uint64) ([]*CustomerCart, error)
}

type repoCustomer struct {
	db *sql.DB
}

func NewRepoCustomer(param store.DBConn) (Contract, error) {
	db, err := store.NewPostgres(param)
	if err != nil {
		return nil, err
	}
	return &repoCustomer{
		db: db,
	}, nil
}
