package repo_merchant

type ItemDetail struct {
	ItemID      uint64
	MerchantID  uint64
	DisplayName string
	Price       uint
	Description string
	Image       string
}

type MerchantBank struct {
	ID                uint64
	MerchantID        uint64
	BankName          string
	BankAccountNumber string
}

type Location struct {
	LocationID uint64
	Province   string
	City       string
}

type MerchantProfile struct {
	MerchantID  uint64
	DisplayName string
	LocationID  uint64
	Description string
	PhotoURL    string
}

type MerchantTransfer struct {
	MerchantID            uint64
	MerchantBankAccountID uint64
	Amount                uint
	TransferID            uint64
}
