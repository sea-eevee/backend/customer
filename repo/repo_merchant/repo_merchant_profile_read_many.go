package repo_merchant

import "database/sql"

func (r repoMerchant) ProfileReaMany() ([]*MerchantProfile, error) {
	var mw []*MerchantProfile

	const query = `SELECT merchant_id, display_name, location_id, description, photo_url
					FROM merchant_profile`

	rows, err := r.db.Query(query)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(MerchantProfile)
		err := rows.Scan(&m.MerchantID, &m.DisplayName, &m.LocationID, &m.Description, &m.PhotoURL)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
