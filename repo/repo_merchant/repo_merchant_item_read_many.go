package repo_merchant

import "database/sql"

func (r repoMerchant) ItemReadManyByMerchantID(merchantID uint64) ([]*ItemDetail, error) {
	var mw []*ItemDetail

	const query = `SELECT id, display_name, price, description, image, merchant_id
					FROM item_detail 
					WHERE merchant_id = $1`

	rows, err := r.db.Query(query, merchantID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(ItemDetail)
		err := rows.Scan(&m.ItemID, &m.DisplayName, &m.Price, &m.Description, &m.Image, &m.MerchantID)
		if err != nil {
			return nil, err
		}
		mw = append(mw, m)
	}

	return mw, nil
}
