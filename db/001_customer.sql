CREATE TABLE customer_profile (
  customer_id bigint NOT NULL, -- REFERENCES customer (id),
  display_name varchar(255) DEFAULT 'blank',
  address varchar(255) DEFAULT 'blank'
);
INSERT INTO customer_profile(customer_id) VALUES (1);
INSERT INTO customer_profile(customer_id) VALUES (2);
INSERT INTO customer_profile(customer_id) VALUES (3);
INSERT INTO customer_profile(customer_id) VALUES (4);
INSERT INTO customer_profile(customer_id) VALUES (5);
INSERT INTO customer_profile(customer_id) VALUES (6);
INSERT INTO customer_profile(customer_id) VALUES (7);

CREATE TABLE customer_shopping_cart (
  id BIGSERIAL PRIMARY KEY,
  customer_id bigint NOT NULL, -- REFERENCES customer (id),
  item_id bigint NOT NULL, -- REFERENCES item (id)
  quantity int DEFAULT 1
);

CREATE TABLE customer_transaction_history (
  id BIGSERIAL PRIMARY KEY,
  customer_id bigint NOT NULL, -- REFERENCES customer (id),
  transaction_id bigint NOT NULL, -- REFERENCES transaction (id)
  is_success boolean
);
