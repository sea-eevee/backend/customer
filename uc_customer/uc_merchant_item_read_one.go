package uc_customer

import (
	"gitlab.com/sea-eevee/backend/customer/pkg/dtos"
)

type MerchantItemReadOneParam struct {
	ItemID uint64 `json:"item_id"`
}

type MerchantItemReadOne struct {
	Item *dtos.Item `json:"item"`
}

type MerchantItemReadOneUCFunc func(param *MerchantItemReadOneParam) (*MerchantItemReadOne, error)

func (u ucCustomer) MerchantItemReadOne(param *MerchantItemReadOneParam) (*MerchantItemReadOne, error) {
	t, err := u.repoMerchant.ItemReadOne(param.ItemID)
	if err != nil {
		return nil, err
	}
	return &MerchantItemReadOne{Item: &dtos.Item{
		ID:          t.ItemID,
		MerchantID:  t.MerchantID,
		DisplayName: t.DisplayName,
		Description: t.Description,
		Price:       t.Price,
		Image:       t.Image,
	}}, nil
}
