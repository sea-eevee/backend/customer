package uc_customer

type MyCartItemUpdateQuantityParam struct {
	CustomerID uint64 `json:"customer_id"`
	ItemID     uint64 `json:"item_id"`
	IsAddOne   bool   `json:"is_add_one"`
}

type MyCartItemUpdateQuantityResponse struct {
}

type MyCartItemUpdateQuantityOneUCFunc func(param *MyCartItemUpdateQuantityParam) (*MyCartItemUpdateQuantityResponse, error)

func (u ucCustomer) MyCartItemUpdateQuantityOne(param *MyCartItemUpdateQuantityParam) (*MyCartItemUpdateQuantityResponse, error) {
	var diff int
	if param.IsAddOne {
		diff = 1
	} else {
		diff = -1
	}

	err := u.repoCustomer.MyCartItemUpdateOne(param.CustomerID, param.ItemID, diff)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
