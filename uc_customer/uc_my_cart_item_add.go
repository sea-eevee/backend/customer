package uc_customer

type MyCartItemAddParam struct {
	CustomerID uint64 `json:"customerID"`
	ItemID     uint64 `json:"item_id"`
	Quantity   uint   `json:"quantity"`
}

type MyCartItemAddResponse struct {
}

type MyCartItemAddUCFunc func(*MyCartItemAddParam) (*MyCartItemAddResponse, error)

func (u ucCustomer) MyCartItemAdd(param *MyCartItemAddParam) (*MyCartItemAddResponse, error) {
	err := u.repoCustomer.MyCartItemAdd(param.CustomerID, param.ItemID, param.Quantity)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
