package uc_customer

import "gitlab.com/sea-eevee/backend/customer/repo/repo_customer"

type ProfileUpdateParam struct {
	CustomerID  uint64 `json:"customer_id"`
	DisplayName string `json:"display_name"`
	Address     string `json:"address"`
}

type ProfileUpdateResponse struct {
	CustomerProfile *repo_customer.CustomerProfile `json:"customer_profile"`
}

type ProfileUpdateUCFunc func(param *ProfileUpdateParam) (*ProfileUpdateResponse, error)

func (u ucCustomer) ProfileUpdate(param *ProfileUpdateParam) (*ProfileUpdateResponse, error) {
	err := u.repoCustomer.ProfileUpdate(param.CustomerID, param.DisplayName, param.Address)
	if err != nil {
		return nil, err
	}
	updatedCustomer, err := u.repoCustomer.ProfileSee(param.CustomerID)
	if err != nil {
		return nil, err
	}
	return &ProfileUpdateResponse{CustomerProfile: updatedCustomer}, nil
}
