package uc_customer

import (
	"gitlab.com/sea-eevee/backend/customer/pkg/dtos"
)

type MerchantReadOneParam struct {
	MerchantID uint64 `json:"merchant_id"`
}

type MerchantReadOneResponse struct {
	Merchant dtos.Merchant `json:"merchant"`
	Items    []*dtos.Item  `json:"items"`
}

type MerchantReadOneUCFunc func(param *MerchantReadOneParam) (*MerchantReadOneResponse, error)

func (u ucCustomer) MerchantReadOne(param *MerchantReadOneParam) (*MerchantReadOneResponse, error) {
	repoMerchant, err := u.repoMerchant.ProfileReadOne(param.MerchantID)
	if err != nil {
		return nil, err
	}

	l, err := u.repoMerchant.LocationReadOne(repoMerchant.LocationID)
	if err != nil {
		return nil, err
	}

	m := dtos.Merchant{
		MerchantID:  repoMerchant.MerchantID,
		DisplayName: repoMerchant.DisplayName,
		Location: dtos.Location{
			LocationID: l.LocationID,
			Province:   l.Province,
			City:       l.City,
		},
		Description: repoMerchant.Description,
		PhotoURL:    repoMerchant.PhotoURL,
	}

	var items []*dtos.Item
	rt, err := u.repoMerchant.ItemReadManyByMerchantID(param.MerchantID)
	if err != nil {
		return nil, err
	}
	for _, t := range rt {
		itemStock, err := u.repoOMS.MerchantItemStockReadOne(t.ItemID)
		if err != nil {
			return nil, err
		}

		item := &dtos.Item{
			ID:          t.ItemID,
			MerchantID:  t.MerchantID,
			DisplayName: t.DisplayName,
			Description: t.Description,
			Price:       t.Price,
			Quantity:    itemStock.Stock,
			Image:       t.Image,
		}
		items = append(items, item)
	}

	return &MerchantReadOneResponse{
		Merchant: m,
		Items:    items,
	}, nil
}
