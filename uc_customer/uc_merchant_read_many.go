package uc_customer

import (
	"gitlab.com/sea-eevee/backend/customer/pkg/dtos"
)

type MerchantReadManyParam struct {
}

type MerchantReadManyResponse struct {
	Merchants []*dtos.Merchant `json:"merchants"`
}

type MerchantReadManyUCFunc func(param *MerchantReadManyParam) (*MerchantReadManyResponse, error)

func (u ucCustomer) MerchantReadMany(_ *MerchantReadManyParam) (*MerchantReadManyResponse, error) {
	repoMerchant, err := u.repoMerchant.ProfileReaMany()
	if err != nil {
		return nil, err
	}

	var merchants []*dtos.Merchant
	for _, rm := range repoMerchant {
		l, err := u.repoMerchant.LocationReadOne(rm.LocationID)
		if err != nil {
			return nil, err
		}

		m := &dtos.Merchant{
			MerchantID:  rm.MerchantID,
			DisplayName: rm.DisplayName,
			Location: dtos.Location{
				LocationID: l.LocationID,
				Province:   l.Province,
				City:       l.City,
			},
			Description: rm.Description,
			PhotoURL:    rm.PhotoURL,
		}

		merchants = append(merchants, m)
	}
	return &MerchantReadManyResponse{Merchants: merchants}, nil
}
