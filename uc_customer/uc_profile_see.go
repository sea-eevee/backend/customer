package uc_customer

import "gitlab.com/sea-eevee/backend/customer/repo/repo_customer"

type ProfileSeeParam struct {
	CustomerID uint64
}

type ProfileSeeResponse struct {
	CustomerProfile *repo_customer.CustomerProfile `json:"customer_profile"`
}

type ProfileSeeUCFunc func(param *ProfileSeeParam) (*ProfileSeeResponse, error)

func (u ucCustomer) ProfileSee(param *ProfileSeeParam) (*ProfileSeeResponse, error) {
	repoCustomer, err := u.repoCustomer.ProfileSee(param.CustomerID)
	if err != nil {
		return nil, err
	}
	return &ProfileSeeResponse{CustomerProfile: repoCustomer}, nil
}
