package uc_customer

import (
	"gitlab.com/sea-eevee/backend/customer/repo/repo_oms"
)

type MyTransactionReadManyParam struct {
	CustomerID uint64
}

type CustomerTransaction struct {
}

type MyTransactionReadManyResponse struct {
	Transactions []*repo_oms.CustomerTransaction `json:"transactions"`
}

type MyTransactionReadMany func(param *MyTransactionReadManyParam) (*MyTransactionReadManyResponse, error)

func (u ucCustomer) MyTransactionReadMany(param *MyTransactionReadManyParam) (*MyTransactionReadManyResponse, error) {
	transactions, err := u.repoOMS.TransactionReadMany(param.CustomerID)
	if err != nil {
		return nil, err
	}
	return &MyTransactionReadManyResponse{Transactions: transactions}, nil
}
