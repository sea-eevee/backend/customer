package uc_customer

import (
	"gitlab.com/sea-eevee/backend/customer/pkg/dtos"
)

type MyCartReadParam struct {
	CustomerID uint64 `json:"customer_id"`
}

type MyCartReadResponse struct {
	Items []*dtos.Item `json:"items"`
}

type MyCartReadUCFunc func(param *MyCartReadParam) (*MyCartReadResponse, error)

func (u ucCustomer) MyCartRead(param *MyCartReadParam) (*MyCartReadResponse, error) {
	repoCarts, err := u.repoCustomer.MyCartRead(param.CustomerID)
	if err != nil {
		return nil, err
	}

	var items []*dtos.Item
	for _, cart := range repoCarts {
		repoItem, err := u.repoMerchant.ItemReadOne(cart.ItemID)
		if err != nil {
			return nil, err
		}
		items = append(items, &dtos.Item{
			ID:          repoItem.ItemID,
			MerchantID:  repoItem.MerchantID,
			DisplayName: repoItem.DisplayName,
			Description: repoItem.Description,
			Price:       repoItem.Price,
			Quantity:    cart.Quantity,
			Image:       repoItem.Image,
		})
	}

	return &MyCartReadResponse{Items: items}, nil
}
