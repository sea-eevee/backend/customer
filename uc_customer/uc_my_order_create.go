package uc_customer

import (
	"errors"
	"gitlab.com/sea-eevee/backend/customer/repo/repo_oms"
)

type MyOrderCreateParam struct {
	CustomerID uint64 `json:"customer_id"`
}

type MyOrderCreateResponse struct {
}

type MyOrderCreateUCFunc func(param *MyOrderCreateParam) (*MyOrderCreateResponse, error)

func (u ucCustomer) MyOrderCreate(param *MyOrderCreateParam) (*MyOrderCreateResponse, error) {
	repoCarts, err := u.repoCustomer.MyCartRead(param.CustomerID)
	if err != nil {
		return nil, err
	}
	err = u.repoCustomer.MyCartRemove(param.CustomerID)
	if err != nil {
		return nil, err
	}
	if len(repoCarts) == 0 {
		return nil, errors.New("err nothing to buy")
	}
	var items []*repo_oms.OrderDetail
	for _, cart := range repoCarts {

		items = append(items, &repo_oms.OrderDetail{
			ItemID:   cart.ItemID,
			Quantity: cart.Quantity,
		})
	}
	item, err := u.repoMerchant.ItemReadOne(repoCarts[0].ItemID)
	if err != nil {
		return nil, err
	}
	err = u.repoOMS.OrderCreate(param.CustomerID, item.MerchantID, items)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
