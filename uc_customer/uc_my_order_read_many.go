package uc_customer

import (
	"gitlab.com/sea-eevee/backend/customer/pkg/dtos"
)

type MyOrderReadManyParam struct {
	CustomerID uint64 `json:"customer_id"`
}

type OrderDetail struct {
	OrderID  uint64     `json:"order_id"`
	Item     *dtos.Item `json:"item"`
	Quantity uint       `json:"quantity"`
}

type CustomerOrder struct {
	OrderID      uint64         `json:"order_id"`
	CustomerID   uint64         `json:"customer_id"`
	OrderDetails []*OrderDetail `json:"order_details"`
	CreatedAt    uint64         `json:"created_at"`
	IsPaid       bool           `json:"is_paid"`
}

type MyOrderReadManyResponse struct {
	CustomerOrder []*CustomerOrder `json:"customer_order"`
}
type MyOrderReadManyUCFunc func(param *MyOrderReadManyParam) (*MyOrderReadManyResponse, error)

func (u ucCustomer) MyOrderReadMany(param *MyOrderReadManyParam) (*MyOrderReadManyResponse, error) {
	orders, err := u.repoOMS.OrderReadMany(param.CustomerID)
	if err != nil {
		return nil, err
	}

	var resp []*CustomerOrder
	for _, order := range orders {

		for _, od := range order.OrderDetails {

			var respOD []*OrderDetail
			idr, err := u.repoMerchant.ItemReadOne(od.ItemID)
			if err != nil {
				return nil, err
			}
			respOD = append(respOD, &OrderDetail{
				OrderID: od.ItemID,
				Item: &dtos.Item{
					ID:          idr.ItemID,
					MerchantID:  idr.MerchantID,
					DisplayName: idr.DisplayName,
					Description: idr.Description,
					Price:       idr.Price,
					Image:       idr.Image,
				},
				Quantity: od.Quantity,
			})

			resp = append(resp, &CustomerOrder{
				OrderID:      order.OrderID,
				CustomerID:   order.CustomerID,
				OrderDetails: respOD,
				CreatedAt:    order.CreatedAt,
				IsPaid:       order.IsPaid,
			})
		}
	}
	return &MyOrderReadManyResponse{CustomerOrder: resp}, nil
}
