package uc_customer

import (
	"gitlab.com/sea-eevee/backend/customer/repo/repo_customer"
	"gitlab.com/sea-eevee/backend/customer/repo/repo_merchant"
	"gitlab.com/sea-eevee/backend/customer/repo/repo_oms"
)

type Contract interface {
	ProfileSee(*ProfileSeeParam) (*ProfileSeeResponse, error)
	ProfileUpdate(*ProfileUpdateParam) (*ProfileUpdateResponse, error)

	MerchantReadMany(*MerchantReadManyParam) (*MerchantReadManyResponse, error)
	MerchantReadOne(*MerchantReadOneParam) (*MerchantReadOneResponse, error)

	MerchantItemReadOne(param *MerchantItemReadOneParam) (*MerchantItemReadOne, error)

	MyCartItemAdd(*MyCartItemAddParam) (*MyCartItemAddResponse, error)
	MyCartItemUpdateQuantityOne(*MyCartItemUpdateQuantityParam) (*MyCartItemUpdateQuantityResponse, error)
	MyCartRead(*MyCartReadParam) (*MyCartReadResponse, error)

	MyOrderCreate(*MyOrderCreateParam) (*MyOrderCreateResponse, error)
	MyOrderReadMany(*MyOrderReadManyParam) (*MyOrderReadManyResponse, error)

	MyTransactionCreate(*MyTransactionCreateParam) (*MyTransactionCreateResponse, error)
	MyTransactionReadMany(*MyTransactionReadManyParam) (*MyTransactionReadManyResponse, error)
}

type ucCustomer struct {
	repoCustomer repo_customer.Contract
	repoMerchant repo_merchant.Contract
	repoOMS      repo_oms.Contract
}

type UCCustomerParam struct {
	RepoCustomer repo_customer.Contract
	RepoMerchant repo_merchant.Contract
	RepoOMS      repo_oms.Contract
}

func NewUCCustomer(param UCCustomerParam) Contract {
	return ucCustomer{
		repoCustomer: param.RepoCustomer,
		repoMerchant: param.RepoMerchant,
		repoOMS:      param.RepoOMS,
	}
}
