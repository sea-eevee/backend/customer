package uc_customer

type MyTransactionCreateParam struct {
	OrderID           uint64 `json:"order_id"`
	BankName          string `json:"bank_name"`
	BankAccountNumber string `json:"bank_account_number"`
}

type MyTransactionCreateResponse struct {
}

type MyTransactionCreateUCFunc func(param *MyTransactionCreateParam) (*MyTransactionCreateResponse, error)

func (u ucCustomer) MyTransactionCreate(param *MyTransactionCreateParam) (*MyTransactionCreateResponse, error) {
	err := u.repoOMS.OrderUpdatePay(param.OrderID)
	if err != nil {
		return nil, nil
	}
	err = u.repoOMS.TransactionCreate(param.OrderID, param.BankName, param.BankAccountNumber)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
