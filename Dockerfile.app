# STAGE 1
FROM golang:1.15-alpine as builder

ENV GO111MODULE=on
WORKDIR /app

RUN apk update && apk add --no-cache git
COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

# STAGE 2
FROM alpine:latest
RUN apk --no-cache add ca-certificates

WORKDIR /root/
COPY --from=builder /app/main .
COPY --from=builder /app/config.env .

EXPOSE 8300
CMD ["./main"]