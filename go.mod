module gitlab.com/sea-eevee/backend/customer

go 1.15

require (
	github.com/gorilla/handlers v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.8.0
	gitlab.com/sea-eevee/backend/common v1.0.1
)
